﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Speech.Synthesis;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Linq;

namespace Speech_Synth
{
	public partial class MainWnd : Form
	{
		private readonly SpeechSynthesizer _synth = new SpeechSynthesizer();

		public MainWnd()
		{
			InitializeComponent();
			_synth.SetOutputToDefaultAudioDevice();

			using (var speaker = new SpeechSynthesizer())
			{
				var voices = speaker.GetInstalledVoices();
				foreach (var voice in voices)
				{
					voiceSelect.Items.Add(new CmVoiceItem(voice));
				}

				_synth.SelectVoice(voices[0].VoiceInfo.Name);
			}

			voiceSelect.SelectedIndex = 0;
			ConfigureDropDownWindowWidth(voiceSelect);
			AcceptButton = sndButton;
		}

		private void sndButton_click(object sender, EventArgs e)
		{
			if (speechInput.Text.Length <= 0) return;
			_synth.SpeakAsyncCancelAll();
			_synth.SpeakAsync(speechInput.Text);
		}

		private void MainWnd_load(object sender, EventArgs e)
		{
			if (!File.Exists(@"saved.json"))
			{
				File.WriteAllText(@"saved.json", @"[]");
			}

			var jo = JArray.Parse(File.ReadAllText(@"saved.json"));
			var v = jo.ToObject<List<string>>();
			dataBox.Items.Clear();
			if (v != null)
			{
				// ReSharper disable once CoVariantArrayConversion
				dataBox.Items.AddRange(v.ToArray());
			}
		}

		private static void ConfigureDropDownWindowWidth(ComboBox control)
		{
			var width = control.DropDownWidth;
			var f = control.Font;

			using (var bmp = new Bitmap(16, 16))
			{
				using (var canvas = Graphics.FromImage(bmp))
				{
					foreach (var item in control.Items)
					{
						var s = item as string;
						if (string.IsNullOrEmpty(s)) continue;
						// Measure the item's width.
						var itemSize = canvas.MeasureString(s, f);
						if (width < itemSize.Width)
						{
							width = (int) itemSize.Width;
						}
					}
				}
			}

			if (width > control.DropDownWidth)
			{
				// Set the width of the drop-down area.
				control.DropDownWidth = width;
			}
		}

		private void addToSavedList_Click(object sender, EventArgs e)
		{
			if (speechInput.Text.Length <= 0)
			{
				return;
			}

			dataBox.Items.Add(speechInput.Text);
			File.WriteAllText(@"saved.json", JArray.FromObject(dataBox.Items).ToString());
		}

		private void dataBox_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			var index = dataBox.IndexFromPoint(e.Location);
			if (index == ListBox.NoMatches) return;
			_synth.SpeakAsyncCancelAll();
			_synth.SpeakAsync(dataBox.Items[index].ToString());
		}

		private void voiceSelect_SelectedIndexChanged(object sender, EventArgs e)
		{
			_synth.SelectVoice(((CmVoiceItem) voiceSelect.Items[voiceSelect.SelectedIndex]).Voice.VoiceInfo.Name);
		}

		private void removeItemBtn_Click(object sender, EventArgs e)
		{
			if (dataBox.SelectedIndex > -1)
			{
				dataBox.Items.RemoveAt(dataBox.SelectedIndex);
			}
		}
	}
}