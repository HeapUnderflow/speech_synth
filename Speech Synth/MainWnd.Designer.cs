﻿namespace Speech_Synth
{
	partial class MainWnd
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.speechInput = new System.Windows.Forms.TextBox();
			this.sndButton = new System.Windows.Forms.Button();
			this.addToSavedList = new System.Windows.Forms.Button();
			this.savedBox = new System.Windows.Forms.GroupBox();
			this.dataBox = new System.Windows.Forms.ListBox();
			this.voiceSelect = new System.Windows.Forms.ComboBox();
			this.removeItemBtn = new System.Windows.Forms.Button();
			this.savedBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// speechInput
			// 
			this.speechInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.speechInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			this.speechInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllSystemSources;
			this.speechInput.Location = new System.Drawing.Point(13, 4);
			this.speechInput.Name = "speechInput";
			this.speechInput.Size = new System.Drawing.Size(594, 20);
			this.speechInput.TabIndex = 0;
			// 
			// sndButton
			// 
			this.sndButton.Location = new System.Drawing.Point(13, 30);
			this.sndButton.Name = "sndButton";
			this.sndButton.Size = new System.Drawing.Size(334, 23);
			this.sndButton.TabIndex = 1;
			this.sndButton.Text = "Speak";
			this.sndButton.UseVisualStyleBackColor = true;
			this.sndButton.Click += new System.EventHandler(this.sndButton_click);
			// 
			// addToSavedList
			// 
			this.addToSavedList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.addToSavedList.Location = new System.Drawing.Point(614, 4);
			this.addToSavedList.Name = "addToSavedList";
			this.addToSavedList.Size = new System.Drawing.Size(27, 20);
			this.addToSavedList.TabIndex = 2;
			this.addToSavedList.Text = "+";
			this.addToSavedList.UseVisualStyleBackColor = true;
			this.addToSavedList.Click += new System.EventHandler(this.addToSavedList_Click);
			// 
			// savedBox
			// 
			this.savedBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.savedBox.Controls.Add(this.removeItemBtn);
			this.savedBox.Controls.Add(this.dataBox);
			this.savedBox.Location = new System.Drawing.Point(13, 59);
			this.savedBox.Name = "savedBox";
			this.savedBox.Size = new System.Drawing.Size(628, 231);
			this.savedBox.TabIndex = 3;
			this.savedBox.TabStop = false;
			this.savedBox.Text = "Saved Entries";
			// 
			// dataBox
			// 
			this.dataBox.FormattingEnabled = true;
			this.dataBox.Location = new System.Drawing.Point(7, 20);
			this.dataBox.Name = "dataBox";
			this.dataBox.Size = new System.Drawing.Size(615, 173);
			this.dataBox.TabIndex = 0;
			this.dataBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dataBox_MouseDoubleClick);
			// 
			// voiceSelect
			// 
			this.voiceSelect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.voiceSelect.FormattingEnabled = true;
			this.voiceSelect.Location = new System.Drawing.Point(353, 30);
			this.voiceSelect.Name = "voiceSelect";
			this.voiceSelect.Size = new System.Drawing.Size(288, 21);
			this.voiceSelect.TabIndex = 4;
			this.voiceSelect.SelectedIndexChanged += new System.EventHandler(this.voiceSelect_SelectedIndexChanged);
			// 
			// removeItemBtn
			// 
			this.removeItemBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.removeItemBtn.Location = new System.Drawing.Point(7, 200);
			this.removeItemBtn.Name = "removeItemBtn";
			this.removeItemBtn.Size = new System.Drawing.Size(615, 23);
			this.removeItemBtn.TabIndex = 1;
			this.removeItemBtn.Text = "Remove selected item.";
			this.removeItemBtn.UseVisualStyleBackColor = true;
			this.removeItemBtn.Click += new System.EventHandler(this.removeItemBtn_Click);
			// 
			// MainWnd
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(653, 302);
			this.Controls.Add(this.voiceSelect);
			this.Controls.Add(this.savedBox);
			this.Controls.Add(this.addToSavedList);
			this.Controls.Add(this.sndButton);
			this.Controls.Add(this.speechInput);
			this.MinimumSize = new System.Drawing.Size(453, 225);
			this.Name = "MainWnd";
			this.Text = "TTS";
			this.Load += new System.EventHandler(this.MainWnd_load);
			this.savedBox.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox speechInput;
		private System.Windows.Forms.Button sndButton;
		private System.Windows.Forms.Button addToSavedList;
		private System.Windows.Forms.GroupBox savedBox;
		private System.Windows.Forms.ComboBox voiceSelect;
		private System.Windows.Forms.ListBox dataBox;
		private System.Windows.Forms.Button removeItemBtn;
	}
}

