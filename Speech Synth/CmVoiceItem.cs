﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;

namespace Speech_Synth
{
	class CmVoiceItem
	{
		public InstalledVoice Voice;

		public CmVoiceItem() {}
		public CmVoiceItem(InstalledVoice voice)
		{
			Voice = voice;
		}

		public override string ToString()
		{
			return $"{Voice.VoiceInfo.Culture} ({Voice.VoiceInfo.Id})";
		}
	}
}
